unit uClasseLogErros;

interface

uses
  SysUtils, Dialogs, Types, Windows, Forms;

type
  TLogErro = class
  private
    FFuncao: String;
    FErro: String;
  public
    function VersaoBuild: String;
    Procedure GravaLog;

    property Erro: String Read FErro Write FErro;
    property Funcao: String Read FFuncao Write FFuncao;
  end;

implementation

{ TLogErro }

procedure TLogErro.GravaLog;
var
  Diretorio: string;
  Arquivo: TextFile;
begin
  // Atribui o endere�o f�sico do
  // arquivo � vari�vel correspondente no Delphi;
  Diretorio := ExtractFilePath(ParamStr(0)) + '\Log.txt';

  // Associa o arquivo � vari�vel do Delphi
  AssignFile(Arquivo, Diretorio);

  if FileExists(Diretorio) then
    { Adiciona linha em branco para
     receber a escrita de dados }
    Append(Arquivo)
  else
    { Recria o arquivo }
    ReWrite(Arquivo);

  try
    WriteLn(Arquivo,'Log - Data: ',
      FormatDateTime('dd/mm/yyyy',Date),
      ' / Hora:', TimeToStr(Time));
    WriteLn(Arquivo,'Tela/Fun��o: ',FFuncao);
    WriteLn(Arquivo,'Mensagem Erro: ',FErro);
    WriteLn(Arquivo,'Vers�o: ',VersaoBuild);
    WriteLn(Arquivo,'----------------------------');
  finally
    CloseFile(arquivo);
  end;
end;

function TLogErro.VersaoBuild: String;
type
  PFFI = ^vs_FixedFileInfo;
var
  F: PFFI;
  Handle: Dword;
  Len: Longint;
  Data: Pchar;
  Buffer: Pointer;
  Tamanho: Dword;
  Parquivo: Pchar;
  Arquivo: String;
begin
  Arquivo := Application.ExeName;
  Parquivo := StrAlloc(Length(Arquivo) + 1);
  StrPcopy(Parquivo, Arquivo);
  Len := GetFileVersionInfoSize(Parquivo, Handle);
  Result := '';

  if Len > 0 then
  begin
    Data := StrAlloc(Len+1);

    if GetFileVersionInfo(Parquivo,Handle,Len,Data) then
    begin
      VerQueryValue(Data,'\',Buffer,Tamanho);
      F := PFFI(Buffer);
      Result := Format('%d.%d.%d.%d',
        [HiWord(F^.dwFileVersionMs),
        LoWord(F^.dwFileVersionMs),
        HiWord(F^.dwFileVersionLs),
        Loword(F^.dwFileVersionLs)]);
    end;

    StrDispose(Data);
  end
  else
    Result := 'Build n�o Habilitado no Delphi.';

  StrDispose(Parquivo);
end;

end.
