object fCadastroUsuario: TfCadastroUsuario
  Left = 371
  Top = 174
  Width = 517
  Height = 415
  Caption = 'Cadastro de Usu'#225'rios'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  DesignSize = (
    501
    377)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 80
    Width = 84
    Height = 13
    Caption = 'Nome Usu'#225'rio:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 144
    Top = 183
    Width = 41
    Height = 13
    Caption = 'Senha:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl1: TLabel
    Left = 288
    Top = 87
    Width = 28
    Height = 13
    Caption = 'CPF:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 24
    Top = 24
    Width = 44
    Height = 13
    Caption = 'C'#243'digo:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 24
    Top = 184
    Width = 36
    Height = 13
    Caption = 'Login:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  inline FraBotoes: TfFrameBotoes
    Left = 90
    Top = 328
    Width = 332
    Height = 34
    Anchors = [akRight, akBottom]
    TabOrder = 6
    inherited btnSair: TButton
      OnClick = btnSairClick
    end
    inherited btnLimpar: TButton
      OnClick = btnLimparClick
    end
    inherited btnExcluir: TButton
      OnClick = btnExcluirClick
    end
    inherited btnGravar: TButton
      Width = 80
      OnClick = btnGravarClick
    end
  end
  object edNome: TEdit
    Left = 24
    Top = 111
    Width = 249
    Height = 21
    TabOrder = 2
  end
  object edSenha: TEdit
    Left = 144
    Top = 207
    Width = 121
    Height = 21
    PasswordChar = '*'
    TabOrder = 4
  end
  object cbMaster: TCheckBox
    Left = 24
    Top = 283
    Width = 121
    Height = 17
    Caption = 'Usu'#225'rio Master'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
  end
  object rgSituacao: TRadioGroup
    Left = 144
    Top = 255
    Width = 194
    Height = 62
    Caption = 'Situa'#231#227'o:'
    Columns = 2
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    Items.Strings = (
      'Ativo'
      'Inativo')
    ParentFont = False
    TabOrder = 5
  end
  object meCPF: TMaskEdit
    Left = 288
    Top = 109
    Width = 114
    Height = 21
    EditMask = '000.000.000-00;1;_'
    MaxLength = 14
    TabOrder = 0
    Text = '   .   .   -  '
  end
  object btnPesquisarPessoa: TButton
    Left = 131
    Top = 44
    Width = 70
    Height = 21
    Caption = 'Pesquisa...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = btnPesquisarPessoaClick
  end
  object Edit1: TEdit
    Left = 24
    Top = 47
    Width = 89
    Height = 21
    TabOrder = 7
  end
  object Edit2: TEdit
    Left = 24
    Top = 207
    Width = 89
    Height = 21
    TabOrder = 8
  end
end
