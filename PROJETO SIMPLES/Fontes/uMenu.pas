unit uMenu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, jpeg, ExtCtrls, uCadastroUsuario,
  uCadastroPessoa;

type
  TfMenu = class(TForm)
    m1: TMainMenu;
    CadastroUsurios1: TMenuItem;
    Sair1: TMenuItem;
    Image1: TImage;
    CadastroPessoas1: TMenuItem;
    Cadastros1: TMenuItem;
    Pessoas1: TMenuItem;
    Faturamento1: TMenuItem;
    Financeiro1: TMenuItem;
    Relatorios1: TMenuItem;
    Config1: TMenuItem;
    Produtos1: TMenuItem;
    NotaEntrada1: TMenuItem;
    NotaSaida1: TMenuItem;
    Pedido1: TMenuItem;
    ContasaPagar1: TMenuItem;
    ContasaReceber1: TMenuItem;
    RelaodeClientes1: TMenuItem;
    RelaodeUsuarios1: TMenuItem;
    RelaodeProdutos1: TMenuItem;
    RelaodeCompras1: TMenuItem;
    RelaodeVendas1: TMenuItem;
    RelaodeContasaPagar1: TMenuItem;
    RelaodeContasaReceber1: TMenuItem;
    FormProdutos: TMenuItem;
    FormGrupoFiscal: TMenuItem;
    EstadoRegiaoMunicipio1: TMenuItem;
    FormPais: TMenuItem;
    FormEstado: TMenuItem;
    FormRegiao: TMenuItem;
    FormMunicipio: TMenuItem;
    FormUnidadeMedida: TMenuItem;
    FormTabeladePrecos: TMenuItem;
    FormFornecedor: TMenuItem;
    FormVendedor: TMenuItem;
    Parametros1: TMenuItem;
    Filial1: TMenuItem;
    procedure Sair1Click(Sender: TObject);
    procedure CadastroUsurios1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CadastroPessoas1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fMenu: TfMenu;

implementation

{$R *.dfm}

procedure TfMenu.Sair1Click(Sender: TObject);
begin
  Close;
end;

procedure TfMenu.CadastroUsurios1Click(Sender: TObject);
begin
  if fCadastroUsuario = nil then
    fCadastroUsuario := TfCadastroUsuario.Create(Self);

  fCadastroUsuario.Show;
end;

procedure TfMenu.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Application.Terminate;
end;

procedure TfMenu.CadastroPessoas1Click(Sender: TObject);
begin
  if fCadastroPessoa = nil then
    fCadastroPessoa := TfCadastroPessoa.Create(Self);
    
  fCadastroPessoa.Show;
end;

end.
