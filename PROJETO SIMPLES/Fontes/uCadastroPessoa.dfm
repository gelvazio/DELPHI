object fCadastroPessoa: TfCadastroPessoa
  Left = 328
  Top = 155
  Width = 388
  Height = 300
  Caption = 'Cadastro de Pessoas'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  DesignSize = (
    372
    262)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 80
    Width = 82
    Height = 13
    Caption = 'Nome Pessoa:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl1: TLabel
    Left = 144
    Top = 23
    Width = 28
    Height = 13
    Caption = 'CPF:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 16
    Top = 122
    Width = 79
    Height = 13
    Caption = 'Observa'#231#245'es:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 16
    Top = 16
    Width = 44
    Height = 13
    Caption = 'C'#243'digo:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object meCPF: TMaskEdit
    Left = 144
    Top = 37
    Width = 118
    Height = 21
    EditMask = '999.999.999-99;0;_'
    MaxLength = 14
    TabOrder = 0
    OnExit = meCPFExit
  end
  object btnPesquisarPessoa: TButton
    Left = 267
    Top = 36
    Width = 78
    Height = 21
    Caption = 'Pesquisa...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = btnPesquisarPessoaClick
  end
  object edNome: TEdit
    Left = 16
    Top = 95
    Width = 337
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 2
  end
  object mObservacao: TMemo
    Left = 16
    Top = 138
    Width = 337
    Height = 49
    Lines.Strings = (
      'mObservacao')
    TabOrder = 3
  end
  inline fFrameBotoes: TfFrameBotoes
    Left = 30
    Top = 236
    Width = 318
    Height = 27
    Anchors = [akRight, akBottom]
    TabOrder = 4
    inherited btnSair: TButton
      OnClick = fFrameBotoesbtnSairClick
    end
    inherited btnLimpar: TButton
      OnClick = fFrameBotoesbtnLimparClick
    end
    inherited btnExcluir: TButton
      OnClick = fFrameBotoesbtnExcluirClick
    end
    inherited btnGravar: TButton
      OnClick = fFrameBotoesbtnGravarClick
    end
  end
  object Edit1: TEdit
    Left = 16
    Top = 39
    Width = 97
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 5
  end
end
