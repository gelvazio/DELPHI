object fPesquisaPessoa: TfPesquisaPessoa
  Left = 358
  Top = 169
  Width = 532
  Height = 416
  Caption = 'Pesquisa Pessoa'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    524
    382)
  PixelsPerInch = 96
  TextHeight = 13
  inline fFrameBotoesPesquisa1: TfFrameBotoesPesquisa
    Left = 336
    Top = 350
    Width = 171
    Height = 30
    Anchors = [akRight, akBottom]
    TabOrder = 0
    inherited btnSelecionar: TButton
      OnClick = btnSelecionarClick
    end
    inherited btnSair: TButton
      OnClick = btnSairClick
    end
  end
  object dbgPessoa: TDBGrid
    Left = 0
    Top = 0
    Width = 524
    Height = 335
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = dsPessoa
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDblClick = btnSelecionarClick
    Columns = <
      item
        Expanded = False
        FieldName = 'NM_PESSOA'
        Title.Caption = 'Nome'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 278
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DT_CAD'
        Title.Caption = 'Data Cadastro'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 95
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CD_CGCCPF'
        Title.Caption = 'CPF'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 118
        Visible = True
      end>
  end
  object dsPessoa: TDataSource
    Left = 16
    Top = 344
  end
end
