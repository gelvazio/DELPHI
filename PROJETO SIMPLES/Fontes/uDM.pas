unit uDM;

interface

uses
  SysUtils, Classes, uClasseBancoDados,
  Dialogs, Forms, uConfiguraBanco, uInicializaSistema;

type
  TDM = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
  public
    BancoDeDados: TBancoDeDados;
  end;

var
  DM: TDM;

implementation

uses uLogOn;

{$R *.dfm}

procedure TDM.DataModuleCreate(Sender: TObject);
begin
  // Esta inst�ncia dever� ficar aberta
  // at� que o sistema seja finalizado
  BancoDeDados := TBancoDeDados.Create;

  if Not(BancoDeDados.ArquivoIniConfiguracaoExiste) then
  begin
    fConfiguraBanco := TfConfiguraBanco.Create(Self);

    try
      fConfiguraBanco.ShowModal;
      BancoDeDados.Diretorio :=Trim(fConfiguraBanco.edDiretorio.Text);
    finally
      fConfiguraBanco.Free;
    end;

    if BancoDeDados.Diretorio = '' then
    begin
      ShowMessage('N�o foi poss�vel configurar o Banco de Dados!');
      OnDestroy(Sender);
      Exit;
    end;

    BancoDeDados.GravaIniConfiguracaoBD;
  end;

  if Not(BancoDeDados.ConectaBancoDeDados) then
  begin
    ShowMessage(' N�o foi poss�vel Conectar no Banco de Dados. '+
                ' Excluir Arquivo de configura��o!             ');
    OnDestroy(Sender);
    Exit;
  end;

  fInicializaSistema := TfInicializaSistema.Create(Self);
  fInicializaSistema.Show;
  fInicializaSistema.Repaint;
  Sleep(2000); // Procedimentos de atualiza��o de banco.
  fInicializaSistema.Close;

  fLogOn := TfLogOn.Create(Self);
  fLogOn.ShowModal;
  Exit;
end;

procedure TDM.DataModuleDestroy(Sender: TObject);
begin
  BancoDeDados.Free;
  Application.Terminate;
  Application.ProcessMessages;
end;

end.
