unit uConfiguraBanco;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, uClasseBancoDados, DB, IBDatabase;

type
  TfConfiguraBanco = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    btnTeste: TButton;
    lbStatus: TLabel;
    Conexao: TIBDatabase;
    btnSair: TButton;
    OpenDialog: TOpenDialog;
    edDiretorio: TEdit;
    btnDialog: TButton;
    procedure btnTesteClick(Sender: TObject);
    procedure btnDialogClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fConfiguraBanco: TfConfiguraBanco;

implementation

{$R *.dfm}

procedure TfConfiguraBanco.btnTesteClick(Sender: TObject);
begin
  if Trim(edDiretorio.Text) = '' then
  begin
    ShowMessage('Banco da Dados n�o selecionado.');
    edDiretorio.SetFocus;
    Exit;
  end;

  try
    try
      lbStatus.Caption := 'Aguarde... Testando Conex�o.';
      Conexao.DatabaseName := Trim(edDiretorio.Text);
      Conexao.Connected := True;
      btnSair.Enabled := True;
      lbStatus.Font.Color := clGreen;
      Repaint;
      lbStatus.Caption := 'Conex�o Ok.';
    except
      lbStatus.Font.Color := clRed;
      Repaint;
      lbStatus.Caption := 'Conex�o n�o efetuada.';
      edDiretorio.SetFocus;
      btnSair.Enabled := False;
    end;
  finally
    Conexao.Connected := False;
  end;
end;

procedure TfConfiguraBanco.btnDialogClick(Sender: TObject);
begin
  OpenDialog.Execute;
  edDiretorio.Text := OpenDialog.Files.Text;
end;

procedure TfConfiguraBanco.btnSairClick(Sender: TObject);
begin
  Close;
end;

end.




