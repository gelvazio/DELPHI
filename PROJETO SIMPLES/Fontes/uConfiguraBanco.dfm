object fConfiguraBanco: TfConfiguraBanco
  Left = 451
  Top = 212
  Width = 383
  Height = 281
  Caption = 'Configura'#231#227'o Banco de Dados'
  Color = 16776176
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 56
    Top = 17
    Width = 210
    Height = 16
    Caption = 'Configurador Banco de Dados'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 22
    Top = 48
    Width = 151
    Height = 13
    Caption = 'Diret'#243'rio Banco de Dados:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbStatus: TLabel
    Left = 8
    Top = 152
    Width = 305
    Height = 16
    Alignment = taCenter
    AutoSize = False
    Caption = 'Status Conex'#227'o'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnTeste: TButton
    Left = 53
    Top = 108
    Width = 103
    Height = 25
    Caption = '&Teste Conex'#227'o'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    OnClick = btnTesteClick
  end
  object btnSair: TButton
    Left = 177
    Top = 108
    Width = 75
    Height = 25
    Caption = '&Sair'
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = btnSairClick
  end
  object edDiretorio: TEdit
    Left = 21
    Top = 64
    Width = 255
    Height = 21
    TabOrder = 2
  end
  object btnDialog: TButton
    Left = 277
    Top = 64
    Width = 22
    Height = 20
    Caption = '...'
    TabOrder = 3
    OnClick = btnDialogClick
  end
  object Conexao: TIBDatabase
    Params.Strings = (
      'user_name=SYSDBA'
      'password=masterkey'
      'lc_ctype=ISO8859_1')
    LoginPrompt = False
    IdleTimer = 0
    SQLDialect = 3
    TraceFlags = []
    Left = 7
    Top = 166
  end
  object OpenDialog: TOpenDialog
    Left = 38
    Top = 165
  end
end
