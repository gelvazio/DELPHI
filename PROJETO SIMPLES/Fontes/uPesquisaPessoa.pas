unit uPesquisaPessoa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, uFrameBotoesPesquisa, Grids, DBGrids, DB;

type
  TfPesquisaPessoa = class(TForm)
    fFrameBotoesPesquisa1: TfFrameBotoesPesquisa;
    dbgPessoa: TDBGrid;
    dsPessoa: TDataSource;
    procedure btnSairClick(Sender: TObject);
    procedure btnSelecionarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    vIDPessoa: Integer;
  end;

var
  fPesquisaPessoa: TfPesquisaPessoa;

implementation

{$R *.dfm}

procedure TfPesquisaPessoa.btnSairClick(Sender: TObject);
begin
  Close;
end;

procedure TfPesquisaPessoa.btnSelecionarClick(Sender: TObject);
begin
  if dsPessoa.DataSet.IsEmpty then
    Exit;

    vIDPessoa := dbgPessoa.DataSource.DataSet.FieldByName('CD_PESSOA').AsInteger;

  Close;
end;

end.
