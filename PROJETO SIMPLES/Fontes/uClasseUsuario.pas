unit uClasseUsuario;

interface

uses
  uClassePessoa, uClasseBancoDados,
  uClasseLogErros, SysUtils, uDM, IBQuery;

type
  TUsuario = class(TPessoa) // Heran�a
  private
    Log: TLogErro;
    SQLExecute: TIBQuery;
    FIDUsuario: Integer;
    FGrupoId: Integer;
    FMaster: Integer;
    FAtivo: Integer;
    FTipoUsuario: Smallint;
    FSenha: string;
    FSQLSelect: TIBQuery;
  public
    function GravarUsuario: Boolean;
    function ExcluirUsuario: Boolean;
    function ListarUsuarios: Boolean;
    function ValidarUsuario: Boolean;
    function BuscarUsuario: Boolean;

    property IDUsuario: Integer read FIDUsuario write FIDUsuario;
    property GrupoID: Integer read FGrupoId write FGrupoId;
    property TipoUsuario: Smallint read FTipoUsuario write FTipoUsuario;

    property Senha: String read FSenha write FSenha;

    property Master: Integer read FMaster write FMaster;
    property Ativo: Integer read FAtivo write FAtivo;
    property SQLSelect: TIBQuery read FSQLSelect;
  end;

implementation

{ TUsuario }

function TUsuario.BuscarUsuario: Boolean;
begin
  Result := False;
  FSQLSelect := DM.BancoDeDados.CriaQuery;

  try
    FSQLSelect.SQL.Add('SELECT                          ');
    FSQLSelect.SQL.Add('	  CD_USUARIO,                 ');
    FSQLSelect.SQL.Add('    DS_USUARIO,                 ');
    FSQLSelect.SQL.Add('    DS_SENHA ,                  ');
    FSQLSelect.SQL.Add('    CD_FILIAL ,                 ');
    FSQLSelect.SQL.Add('    DT_ALT,                     ');
    FSQLSelect.SQL.Add('    HR_ALT,                     ');
    FSQLSelect.SQL.Add('    DT_CAD,                     ');
    FSQLSelect.SQL.Add('    HR_CAD ,                    ');
    FSQLSelect.SQL.Add('FROM                            ');
    FSQLSelect.SQL.Add('		USUARIO                     ');
    FSQLSelect.SQL.Add('Where CD_USUARIO = :PID         ');
    FSQLSelect.ParamByName('PID').Value := FIDUsuario;

    try
      FSQLSelect.Open;

      if not(FSQLSelect.IsEmpty) then
      begin

      {

            vIDUsuario := vUsuario.IDUsuario;
            rgSituacao.ItemIndex := vUsuario.Ativo;
            cbMaster.Checked := vUsuario.Master = 1;
            edSenha.Text := vUsuario.Senha;


        FGrupoId := FSQLSelect.FieldByName('Grupo_ID').AsInteger;
        FMaster := FSQLSelect.FieldByName('Master').AsInteger;
        FAtivo := FSQLSelect.FieldByName('Ativo').AsInteger;
        FTipoUsuario := FSQLSelect.FieldByName('TipoUsuario').AsInteger;
       }

        FSenha := FSQLSelect.FieldByName('DS_SENHA').AsString;
        Result := True;
      end;
    except on e:Exception do
      begin
        Log := TLogErro.Create;

        try
          Log.Erro := e.Message;
          Log.Funcao := 'TUsuario.BuscarUsuarios';
          Log.GravaLog;
        finally
          Log.Free;
        end;
      end;
    end;
  finally
    FSQLSelect.Free;
  end;
end;

function TUsuario.ExcluirUsuario: Boolean;
begin
  Result := False;
  SQLExecute := DM.BancoDeDados.CriaQuery;

  if not(DM.BancoDeDados.Transacao.InTransaction) then
    DM.BancoDeDados.Transacao.StartTransaction;

  try
    SQLExecute.SQL.Add('DELETE FROM USUARIO ');
    SQLExecute.SQL.Add('WHERE CD_USUARIO = :PID');
    SQLExecute.ParamByName('PID').Value := FIDUsuario;

    try
      SQLExecute.ExecSQL;
      DM.BancoDeDados.Transacao.Commit;
      Result := True;
    except on e:Exception do
      begin
        DM.BancoDeDados.Transacao.Rollback;
        Log := TLogErro.Create;

        try
          Log.Erro := e.Message;
          Log.Funcao := 'TUsuario.ExcluirUsuario';
          Log.GravaLog;
        finally
          Log.Free;
        end;
      end;
    end;
  finally
    SQLExecute.Free;
  end;
end;

function TUsuario.GravarUsuario: Boolean;
begin
  Result := False;
  SQLExecute := DM.BancoDeDados.CriaQuery;

  if not(DM.BancoDeDados.Transacao.InTransaction) then
    DM.BancoDeDados.Transacao.StartTransaction;

  try
    if FIDUsuario = 0 then
    begin
      FIDUsuario := IDPessoa;
      SQLExecute.SQL.Add('Insert into Usuario');
      SQLExecute.SQL.Add('(ID, Grupo_ID, Master, Ativo,');
      SQLExecute.SQL.Add(' TipoUsuario, Senha)');
      SQLExecute.SQL.Add('Values');
      SQLExecute.SQL.Add('(:PID, :PGrupoID, :PMaster,');
      SQLExecute.SQL.Add(' :PAtivo, :PTipoUsuario,');
      SQLExecute.SQL.Add(' :PSenha)');
    end
    else
    begin
      SQLExecute.SQL.Add('Update Usuario Set');
      SQLExecute.SQL.Add('Grupo_ID = :PGrupoID,');
      SQLExecute.SQL.Add('Master = :PMaster,');
      SQLExecute.SQL.Add('Ativo = :PAtivo,');
      SQLExecute.SQL.Add('TipoUsuario = :PTipoUsuario,');
      SQLExecute.SQL.Add('Senha = :PSenha');
      SQLExecute.SQL.Add('Where ID = :PID');
    end;

    SQLExecute.ParamByName('PID').Value := FIDUsuario;
    SQLExecute.ParamByName('PGrupoID').Value := FGrupoId;
    SQLExecute.ParamByName('PMaster').Value := FMaster;
    SQLExecute.ParamByName('PAtivo').Value := FAtivo;
    SQLExecute.ParamByName('PTipoUsuario').Value := FTipoUsuario;
    SQLExecute.ParamByName('PSenha').Value := FSenha;

    try
      SQLExecute.ExecSQL;
      DM.BancoDeDados.Transacao.Commit;
      Result := True;
    except on e:Exception do
      begin
        DM.BancoDeDados.Transacao.Rollback;
        
        Log := TLogErro.Create;

        try
          Log.Erro := e.Message;
          Log.Funcao := 'TUsuario.GravarUsuario';
          Log.GravaLog;
        finally
          Log.Free;
        end;
      end;
    end;
  finally
    SQLExecute.Free;
  end;
end;

function TUsuario.ListarUsuarios: Boolean;
begin
  Result := False;
  FSQLSelect := DM.BancoDeDados.CriaQuery;

    FSQLSelect.SQL.Add('SELECT                ');
    FSQLSelect.SQL.Add('	  CD_USUARIO,       ');
    FSQLSelect.SQL.Add('    DS_USUARIO,       ');
    FSQLSelect.SQL.Add('    DS_SENHA ,        ');
    FSQLSelect.SQL.Add('    CD_FILIAL ,       ');
    FSQLSelect.SQL.Add('    DT_ALT,           ');
    FSQLSelect.SQL.Add('    HR_ALT,           ');
    FSQLSelect.SQL.Add('    DT_CAD,           ');
    FSQLSelect.SQL.Add('    HR_CAD            ');
    FSQLSelect.SQL.Add('FROM                  ');
    FSQLSelect.SQL.Add('		USUARIO           ');

  try
    FSQLSelect.Open;
    Result := True;
  except on e:Exception do
    begin
      Log := TLogErro.Create;
      try
        Log.Erro := e.Message;
        Log.Funcao := 'TUsuario.ListarUsuarios';
        Log.GravaLog;
      finally
        Log.Free;
      end;
    end;
  end;
end;

function TUsuario.ValidarUsuario: Boolean;
begin
  Result := False;
  FSQLSelect := DM.BancoDeDados.CriaQuery;

  try
    FSQLSelect.SQL.Add('SELECT                                          ');
    FSQLSelect.SQL.Add('    USUARIO.DS_USUARIO,                         ');
    FSQLSelect.SQL.Add('    USUARIO.DS_SENHA                            ');
    FSQLSelect.SQL.Add('FROM                                            ');
    FSQLSelect.SQL.Add('    USUARIO                                     ');
    FSQLSelect.SQL.Add('    INNER JOIN PESSOA_SIMPLES ON                ');
    FSQLSelect.SQL.Add('    PESSOA_SIMPLES.CD_PESSOA=USUARIO.CD_USUARIO ');
    FSQLSelect.SQL.Add('WHERE                                           ');
    {
      MUITO IMPORTANTE!!!
      OS DADOS DE PARAMETROS DEVEM SER SEMPRE PROXIMOS!!
      EXEMPLO:
      USUARIO.DS_SENHA = : FSenha -->>DA ERRO!!
      USUARIO.DS_SENHA =:FSenha --> FICA CERTO!!
    }

    FSQLSelect.SQL.Add('    PESSOA_SIMPLES.NM_PESSOA =:FNome            ');
    FSQLSelect.SQL.Add('    AND                                         ');
    FSQLSelect.SQL.Add('    USUARIO.DS_SENHA =:FSenha                   ');
    FSQLSelect.ParamByName('FNome').AsString := Nome;
    {
      Comentario em rela��o ao parametro Senha.
      Este parametro tem o nome de "FSenha" por que nao instancia
      de outra classe e sim pega direto desta classe.
    }
    FSQLSelect.ParamByName('FSenha').AsString := FSenha;

    try
      FSQLSelect.Open;

      if Not(FSQLSelect.IsEmpty) then
        Result := True;
    except on e:Exception do
      begin
        Log := TLogErro.Create;
        try
          Log.Erro := e.Message;
          Log.Funcao := 'TUsuario.ValidarUsuario';
          Log.GravaLog;
        finally
          Log.Free;
        end;
      end;
    end;
  finally
    FSQLSelect.Free;
  end;
end;

end.
