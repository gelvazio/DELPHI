unit uClasseCriptografia;

interface

type
  TCriptografia = class
  private
    FDadosCriptografia: String;
    FTipoCriptografia: String;
  public
    function Criptografa: String;
    property DadosCriptografia: String Read FDadosCriptografia Write FDadosCriptografia;
    property TipoCriptografia: String Read FTipoCriptografia Write FTipoCriptografia;
  end;

implementation

{ TCriptografia }

function TCriptografia.Criptografa: String;
var
  Cripto :String;
  i, ate, x1, x2: integer;
  r1, r2: string[1];
begin
  Cripto := '';

  if Odd(Length(FDadosCriptografia)) then
    FDadosCriptografia := FDadosCriptografia + ' ';

  ate := Length(FDadosCriptografia) div 2;

  for i := 1 to ate do
  begin
    x1 := ord(FDadosCriptografia[((i-1) * 2) + 1]);
    x2 := ord(FDadosCriptografia[((i-1) * 2) + 2]);
    if TipoCriptografia = 'D' then
    begin
      r2 := chr((-x2 + x1 + 157) div 2);
      r1 := chr(x1 - ((-x2 + x1 + 157) div 2));
    end
    else
    if TipoCriptografia = 'C' then
    begin
      r2 := chr(x1 + 157 - x2);
      r1 := chr(x1 + x2);
    end;
    Cripto := Cripto + r1 + r2;
  end;

  Result := Cripto;
end;

end.
