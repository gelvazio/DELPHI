unit uClasseBancoDados;

interface

uses
  SysUtils, uClasseLogErros, uClasseCriptografia,
  Forms, Classes, IniFiles, IBDatabase,
  IBQuery, dialogs;

type
  TBancoDeDados = class
  private
    Log: TLogErro;// Referência da classe
    Criptografia: TCriptografia;
    ArquivoIni: TIniFile;
    FConexaoBD: TIBDatabase;
    FUsuario: String;
    FSenha: String;
    FCharset: String;
    FDiretorio: String;
    FTransacao: TIBTransaction;
  public
    function ConectaBancoDeDados: Boolean;
    function ArquivoIniConfiguracaoExiste: Boolean;
    function CriaQuery: TIBQuery;
    procedure CarregaIniConfiguracaoBD;
    procedure DesconectaBancoDeDados;
    procedure GravaIniConfiguracaoBD;

    property ConexaoBD: TIBDatabase read FConexaoBD;
    property Transacao: TIBTransaction read FTransacao;
    property Usuario: String read FUsuario write FUsuario;
    property Senha: String read FSenha write FSenha;
    property Charset: String read FCharset write FCharset;
    property Diretorio: String read FDiretorio write FDiretorio;
  end;

implementation

{ TBancoDeDados }


procedure TBancoDeDados.DesconectaBancoDeDados;
begin
  if FConexaoBD <> nil then
  begin
    FConexaoBD.Connected := False;
    FConexaoBD.Free;
  end;
end;

function TBancoDeDados.ConectaBancoDeDados: Boolean;
begin
  Result := False;
  FConexaoBD := TIBDatabase.Create(nil);
  FTransacao := TIBTransaction.Create(nil);
  CarregaIniConfiguracaoBD;

  try
    // Configuração Banco de Dados
    FConexaoBD.DatabaseName := FDiretorio;
    FConexaoBD.Params.Add(FUsuario);
    FConexaoBD.Params.Add(FSenha);
    FConexaoBD.Params.Add(FCharset);
    FConexaoBD.LoginPrompt := False;
    FConexaoBD.SQLDialect := 3;

    // Configuração Transaction
    FConexaoBD.DefaultTransaction := FTransacao;
    FTransacao.DefaultAction := TACommit;
    FTransacao.AutoStopAction := saCommit;

    FConexaoBD.Connected := True;
    Result := True;
  except on e:Exception do
    begin
      Log := TLogErro.Create;

      try
        Log.Erro := e.Message;
        Log.Funcao := 'TBancoDeDados.Conecta';
        Log.GravaLog;
      finally
        Log.Free;
      end;

      DesconectaBancoDeDados;
    end;
  end;
end;

procedure TBancoDeDados.GravaIniConfiguracaoBD;
begin
  ArquivoIni := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'Controle.ini');
  Criptografia := TCriptografia.Create;

  try
    Criptografia.TipoCriptografia := 'C'; // Criptografa

    // Parâmetros pré-fixados (Internos)
    FUsuario := 'user_name=SYSDBA';
    Criptografia.DadosCriptografia := FUsuario;
    ArquivoIni.WriteString('CONEXAO','USUARIO',Criptografia.Criptografa);

    FSenha := 'password=masterkey';
    Criptografia.DadosCriptografia := FSenha;
    ArquivoIni.WriteString('CONEXAO','SENHA',Criptografia.Criptografa);

    FCharset := 'lc_ctype=ISO8859_1';
    Criptografia.DadosCriptografia := FCharset;
    ArquivoIni.WriteString('CONEXAO','CHARSET',Criptografia.Criptografa);

    // Parâmetro configurável (Externo)
    Criptografia.DadosCriptografia := FDiretorio;
    ArquivoIni.WriteString('CONEXAO','DIRETORIO',Criptografia.Criptografa);
  finally
    ArquivoIni.Free;
    Criptografia.Free;
  end;
end;

procedure TBancoDeDados.CarregaIniConfiguracaoBD;
begin
  ArquivoIni := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'Controle.ini');
  Criptografia := TCriptografia.Create;

  try
    Criptografia.TipoCriptografia := 'D'; // Descriptografa

    Criptografia.DadosCriptografia := ArquivoIni.ReadString('CONEXAO','USUARIO','');
    FUsuario := Criptografia.Criptografa;

    Criptografia.DadosCriptografia := ArquivoIni.ReadString('CONEXAO','SENHA','');
    FSenha := Criptografia.Criptografa;

    Criptografia.DadosCriptografia := ArquivoIni.ReadString('CONEXAO','CHARSET','');
    FCharset := Criptografia.Criptografa;

    Criptografia.DadosCriptografia := ArquivoIni.ReadString('CONEXAO','DIRETORIO','');
    FDiretorio := Criptografia.Criptografa;
  finally
    ArquivoIni.Free;
    Criptografia.Free;
  end;
end;

function TBancoDeDados.ArquivoIniConfiguracaoExiste: Boolean;
begin
  Result := False;

  if FileExists(ExtractFilePath(ParamStr(0)) +
     'Controle.ini') then
    Result := True;
end;

function TBancoDeDados.CriaQuery: TIBQuery;
var
  Query: TIBQuery;
begin
  Query := TIBQuery.Create(nil);
  Query.Database := FConexaoBD;
  Query.Close;
  Query.SQL.Clear;
  Result := Query;
end;

end.



