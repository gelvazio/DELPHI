unit uInicializa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, uClasseBancoDados;

type
  TfInicializa = class(TForm)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    BancoDeDados: TBancoDeDados;
  public
    { Public declarations }
  end;

var
  fInicializa: TfInicializa;

implementation

{$R *.dfm}

procedure TfInicializa.FormCreate(Sender: TObject);
begin
  // Inicializa Conex�o Banco de Dados.
  BancoDeDados := TBancoDeDados.Create;

  try
    if BancoDeDados.ConectaBancoDeDados then
      ShowMessage('Conex�o Ok!')
    else
      ShowMessage('N�o foi poss�vel conectar o sistema ao Banco de Dados.');
  finally
    BancoDeDados.Free;
  end;
end;

end.
