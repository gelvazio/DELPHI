object fPesquisaUsuario: TfPesquisaUsuario
  Left = 343
  Top = 191
  Width = 532
  Height = 436
  Caption = 'Pesquisa Usu'#225'rio'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    524
    402)
  PixelsPerInch = 96
  TextHeight = 13
  inline fFrameBotoesPesquisa1: TfFrameBotoesPesquisa
    Left = 337
    Top = 369
    Width = 171
    Height = 30
    Anchors = [akRight, akBottom]
    TabOrder = 0
    inherited btnSair: TButton
      OnClick = btnSairClick
    end
  end
  object dbgUsuario: TDBGrid
    Left = 0
    Top = 0
    Width = 524
    Height = 289
    Align = alTop
    DataSource = dsUsuario
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDblClick = dbgUsuarioDblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'CD_USUARIO'
        Title.Caption = 'CODIGO'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DS_USUARIO'
        Title.Caption = 'NOME'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DS_SENHA'
        Title.Caption = 'SENHA'
        Visible = True
      end>
  end
  object dsUsuario: TDataSource
    Left = 32
    Top = 320
  end
end
