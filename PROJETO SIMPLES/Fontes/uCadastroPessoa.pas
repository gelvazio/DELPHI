unit uCadastroPessoa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, uFrameBotoes, uClassePessoa,
  uPesquisaPessoa;

type
  TfCadastroPessoa = class(TForm)
    meCPF: TMaskEdit;
    btnPesquisarPessoa: TButton;
    Label1: TLabel;
    edNome: TEdit;
    lbl1: TLabel;
    mObservacao: TMemo;
    Label2: TLabel;
    fFrameBotoes: TfFrameBotoes;
    Edit1: TEdit;
    Label3: TLabel;
    procedure fFrameBotoesbtnSairClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure fFrameBotoesbtnLimparClick(Sender: TObject);
    procedure fFrameBotoesbtnExcluirClick(Sender: TObject);
    procedure fFrameBotoesbtnGravarClick(Sender: TObject);
    procedure btnPesquisarPessoaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure meCPFExit(Sender: TObject);
  private
    { Private declarations }
    vPessoa: TPessoa;
    vIDPessoa: Integer;
    procedure LimparTela;
  public
    { Public declarations }
  end;

var
  fCadastroPessoa: TfCadastroPessoa;

implementation

{$R *.dfm}

procedure TfCadastroPessoa.fFrameBotoesbtnSairClick(Sender: TObject);
begin
  Close;
end;

procedure TfCadastroPessoa.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
  fCadastroPessoa := Nil;
end;

procedure TfCadastroPessoa.LimparTela;
begin
  vIDPessoa := 0;
  meCPF.Clear;
  edNome.Clear;
  mObservacao.Clear;
end;

procedure TfCadastroPessoa.fFrameBotoesbtnLimparClick(Sender: TObject);
begin
  LimparTela;
  meCPF.SetFocus;
end;

procedure TfCadastroPessoa.fFrameBotoesbtnExcluirClick(Sender: TObject);
begin
  ShowMessage('Programar M�todo de Excluir a Pessoa!');

{

  if vIDPessoa = 0 then
  begin
    ShowMessage('Registro n�o selecionado para exclus�o');
    meCPF.SetFocus;
    Exit;
  end;

  if MessageDlg('Confirma a exclus�o da Pessoa?',
    mtConfirmation,[mbYes,mbNo],0) = mrNo then
  begin
    meCPF.SetFocus;
    Exit;
  end;

  vPessoa := TPessoa.Create;

  try
    vPessoa.IDPessoa := vIDPessoa;

    if vPessoa.ExcluirPessoa then
    begin
      ShowMessage('Pessoa excluida com sucesso.');
      LimparTela;
    end
    else
      ShowMessage('N�o foi poss�vel excluir a pessoa.');

    meCPF.SetFocus;
  finally
    vPessoa.Free;
  end;

}  
end;

procedure TfCadastroPessoa.fFrameBotoesbtnGravarClick(Sender: TObject);
begin
  ShowMessage('Programar M�todo de Gravar a Pessoa!');
{

  if Trim(meCPF.Text) = '' then
  begin
    ShowMessage('CPF n�o selecionado.');
    meCPF.SetFocus;
    Exit;
  end;

  if Trim(edNome.Text) = '' then
  begin
    ShowMessage('Nome n�o informado.');
    edNome.SetFocus;
    Exit;
  end;

  vPessoa := TPessoa.Create;

  try
    vPessoa.IDPessoa := vIDPessoa;
    vPessoa.CPF := meCPF.Text;
    vPessoa.Logradouro_ID := 0;
    vPessoa.DataCadastro := Date;
    vPessoa.Nome := edNome.Text;
    vPessoa.Observacao := mObservacao.Text;

    if vPessoa.GravarPessoa then
    begin
      ShowMessage('Pessoa gravada com sucesso.');
      LimparTela;
    end
    else
      ShowMessage('N�o foi poss�vel gravar a pessoa.');

    meCPF.SetFocus;
  finally
    vPessoa.Free;
  end;

}


end;

procedure TfCadastroPessoa.btnPesquisarPessoaClick(Sender: TObject);
begin
  LimparTela;

  if fPesquisaPessoa = nil then
    fPesquisaPessoa := TfPesquisaPessoa.Create(Self);

  vPessoa := TPessoa.Create;

  try
    if vPessoa.ListarPessoas then
      fPesquisaPessoa.dsPessoa.DataSet := vPessoa.SQLSelect;

    fPesquisaPessoa.ShowModal;

    if fPesquisaPessoa.vIDPessoa > 0 then
    begin
      vPessoa.IDPessoa := fPesquisaPessoa.vIDPessoa;

      if vPessoa.BuscarPessoa then
      begin
        vIDPessoa := vPessoa.IDPessoa;
        meCPF.Text := vPessoa.CPF;
        edNome.Text := vPessoa.Nome;
        mObservacao.Text := vPessoa.Observacao;
      end;
    end;
  finally
    vPessoa.Free;
    fPesquisaPessoa.Free;
    fPesquisaPessoa := Nil;
  end;
end;

procedure TfCadastroPessoa.FormCreate(Sender: TObject);
begin
  LimparTela;
end;

procedure TfCadastroPessoa.meCPFExit(Sender: TObject);
begin
  vPessoa := TPessoa.Create;

  try
    vPessoa.CPF := meCPF.Text;

    if vPessoa.ValidarPessoa then
    begin
      ShowMessage('Pessoa J� Existente.');
      vIDPessoa := vPessoa.IDPessoa;
      meCPF.Text := vPessoa.CPF;
      edNome.Text := vPessoa.Nome;
      mObservacao.Text := vPessoa.Observacao;
    end;
  finally
    vPessoa.Free;
  end;
end;

end.
