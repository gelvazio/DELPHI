unit uPesquisaUsuario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, uFrameBotoesPesquisa, Grids, DBGrids, DB;

type
  TfPesquisaUsuario = class(TForm)
    fFrameBotoesPesquisa1: TfFrameBotoesPesquisa;
    dbgUsuario: TDBGrid;
    dsUsuario: TDataSource;
    procedure btnSairClick(Sender: TObject);
    procedure dbgUsuarioDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    vIDUsuario: Integer;
  end;

var
  fPesquisaUsuario: TfPesquisaUsuario;

implementation

{$R *.dfm}

procedure TfPesquisaUsuario.btnSairClick(Sender: TObject);
begin
  Close;
end;



procedure TfPesquisaUsuario.dbgUsuarioDblClick(Sender: TObject);
begin
  if dsUsuario.DataSet.IsEmpty then
    Exit;

    vIDUsuario := dbgUsuario.DataSource.DataSet.FieldByName('CD_USUARIO').AsInteger;

  Close;
end;

end.
