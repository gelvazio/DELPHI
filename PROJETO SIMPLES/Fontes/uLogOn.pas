unit uLogOn;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, jpeg, ExtCtrls, uClasseUsuario, uClassePessoa, IBQuery,
  uClasseBancoDados, uMenu;

type
  TfLogOn = class(TForm)
    img1: TImage;
    lbl1: TLabel;
    lbl2: TLabel;
    edSenha: TEdit;
    btnEntrar: TButton;
    btnSair: TButton;
    cbUsuario: TComboBox;
    procedure btnSairClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnEntrarClick(Sender: TObject);
  private
    { Private declarations }
    Usuario: TUsuario;
  public
    { Public declarations }
  end;

var
  fLogOn: TfLogOn;

implementation

uses DB;

{$R *.dfm}

procedure TfLogOn.btnSairClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfLogOn.FormCreate(Sender: TObject);
begin
  Usuario := TUsuario.Create;

  try
    if Usuario.ListarUsuarios then
    begin
      Usuario.SQLSelect.First;

      while not(Usuario.SQLSelect.Eof) do
      begin
        Usuario.IDPessoa :=Usuario.SQLSelect.FieldByName('CD_USUARIO').AsInteger;

        if Usuario.BuscarPessoa then
          cbUsuario.Items.Add(Usuario.Nome);

        Usuario.SQLSelect.Next;
      end;
    end;
  finally
    Usuario.Free;
  end;
end;

procedure TfLogOn.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfLogOn.btnEntrarClick(Sender: TObject);
begin
  Usuario := TUsuario.Create;

  try
    Usuario.Nome  :=cbUsuario.Items.Strings[cbUsuario.itemindex];
    Usuario.Senha :=edSenha.Text;

    if Usuario.ValidarUsuario then
    begin
      fMenu := TfMenu.Create(Self);
      fMenu.Show;
      fLogOn.Hide;
    end
    else
    begin
      ShowMessage('Dados de acesso incorretos.');
    end;
  finally
    Usuario.Free;
  end;
end;

end.
