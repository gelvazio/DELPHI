unit uCadastroUsuario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes,
  Graphics, Controls, Forms, Dialogs, uFrameBotoes,
  StdCtrls, ExtCtrls, Mask, uClassePessoa,
  uClasseUsuario, uClasseCriptografia,
  uPesquisaPessoa, uPesquisaUsuario;

type
  TfCadastroUsuario = class(TForm)
    FraBotoes: TfFrameBotoes;
    edNome: TEdit;
    Label1: TLabel;
    edSenha: TEdit;
    Label2: TLabel;
    cbMaster: TCheckBox;
    rgSituacao: TRadioGroup;
    lbl1: TLabel;
    meCPF: TMaskEdit;
    btnPesquisarPessoa: TButton;
    Edit1: TEdit;
    Label3: TLabel;
    Edit2: TEdit;
    Label4: TLabel;
    procedure btnSairClick(Sender: TObject);
    procedure btnGravarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnLimparClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnPesquisarPessoaClick(Sender: TObject);
  private
    { Private declarations }
    vIDUsuario: Integer;
    vUsuario: TUsuario;
    vPessoa: TPessoa;
    procedure Limpartela;
  public
    { Public declarations }
  end;

var
  fCadastroUsuario: TfCadastroUsuario;

implementation

{$R *.dfm}

procedure TfCadastroUsuario.btnSairClick(Sender: TObject);
begin
  Close;
end;

procedure TfCadastroUsuario.Limpartela;
begin
  vIDUsuario := 0;
  meCPF.Clear;
  edNome.Clear;
  edSenha.Clear;
  cbMaster.Checked := False;
  rgSituacao.ItemIndex := -1;
end;

procedure TfCadastroUsuario.btnGravarClick(Sender: TObject);
begin
  if Trim(meCPF.Text) = '' then
  begin
    ShowMessage('CPF n�o selecionado.');
    meCPF.SetFocus;
    Exit;
  end;

  if Trim(edNome.Text) = '' then
  begin
    ShowMessage('Nome n�o informado.');
    edNome.SetFocus;
    Exit;
  end;

  if Trim(edSenha.Text) = '' then
  begin
    ShowMessage('Senha n�o informada.');
    edSenha.SetFocus;
    Exit;
  end;

  vUsuario := TUsuario.Create;

  try
    vUsuario.CPF := Trim(meCPF.Text);

    if vUsuario.ValidarPessoa then
    begin
      vUsuario.IDUsuario := vIDUsuario;
      vUsuario.GrupoID := 0;
      vUsuario.TipoUsuario := 1;
      vUsuario.Senha := edSenha.Text;

      if cbMaster.Checked then
        vUsuario.Master := 1
      else
        vUsuario.Master := 0;

      vUsuario.Ativo := rgSituacao.ItemIndex;

      if vUsuario.GravarUsuario then
      begin
        ShowMessage('Usu�rio gravado com sucesso.');
        Limpartela;
      end
      else
        ShowMessage('N�o foi poss�vel gravar o usu�rio.');
        
      meCPF.SetFocus;
    end
    else
    begin
      ShowMessage('Pessoa n�o cadastrada.');
      Exit;
    end;
  finally
    vUsuario.Free;
  end;
end;

procedure TfCadastroUsuario.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
  fCadastroUsuario := nil;
end;

procedure TfCadastroUsuario.btnLimparClick(Sender: TObject);
begin
  Limpartela;
  meCPF.SetFocus;
end;

procedure TfCadastroUsuario.FormCreate(Sender: TObject);
begin
  Limpartela;
end;

procedure TfCadastroUsuario.btnExcluirClick(Sender: TObject);
begin
  if vIDUsuario = 0 then
  begin
    ShowMessage('Registro n�o selecionado para exclus�o');
    meCPF.SetFocus;
    Exit;
  end;

  if MessageDlg('Confirma a exclus�o do usu�rio selecionado?',
    mtConfirmation, [mbYes,mbNo],0) = mrNo then
  begin
    meCPF.SetFocus;
    Exit;
  end;

  vUsuario := TUsuario.Create;

  try
    vUsuario.IDUsuario := vIDUsuario;

    if vUsuario.ExcluirUsuario then
    begin
      ShowMessage('Usu�rio exclu�do com sucesso');
      Limpartela;
    end
    else
      ShowMessage('N�o foi poss�vel excluir o '+
                  'usus�rio selecionado.');

    meCPF.SetFocus;
  finally
    vUsuario.Free;
  end;
end;

procedure TfCadastroUsuario.btnPesquisarPessoaClick(Sender: TObject);
begin

  Limpartela;

  if fPesquisaUsuario = nil then
    fPesquisaUsuario := TfPesquisaUsuario.Create(Self);

  vUsuario := TUsuario.Create;

  try
    if vUsuario.ListarUsuarios then
      fPesquisaUsuario.dsUsuario.DataSet := vUsuario.SQLSelect;

    fPesquisaUsuario.ShowModal;
   finally
    vUsuario.Free;
    fPesquisaUsuario.Free;
    fPesquisaUsuario := nil;
end;    

{
PROGRAMACAO VELHA

  Limpartela;

  if fPesquisaPessoa = nil then
    fPesquisaPessoa := TfPesquisaPessoa.Create(Self);

  vPessoa := TPessoa.Create;

  try
    if vPessoa.ListarPessoas then
      fPesquisaPessoa.dsPessoa.DataSet := vPessoa.SQLSelect;
      
    fPesquisaPessoa.ShowModal;

    if fPesquisaPessoa.vIDPessoa > 0 then
    begin
      vPessoa.IDPessoa := fPesquisaPessoa.vIDPessoa;

      if vPessoa.BuscarPessoa then
      begin
        meCPF.Text := vPessoa.CPF;
        edNome.Text := vPessoa.Nome;

        vUsuario := TUsuario.Create;

        try
          vUsuario.IDUsuario := fPesquisaPessoa.vIDPessoa;

          if vUsuario.BuscarUsuario then
          begin
            vIDUsuario := vUsuario.IDUsuario;
            //rgSituacao.ItemIndex := vUsuario.Ativo;
            //cbMaster.Checked := vUsuario.Master = 1;
            edSenha.Text := vUsuario.Senha;
          end
          else
            vIDUsuario := 0;
        finally
          vUsuario.Free;
        end;
      end
      else
        Limpartela;
    end;
  finally
    vPessoa.Free;
    fPesquisaPessoa.Free;
    fPesquisaPessoa := nil;
  end;
}  
end;

end.
