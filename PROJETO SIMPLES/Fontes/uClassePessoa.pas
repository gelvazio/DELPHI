unit uClassePessoa;

interface

uses
  uClasseBancoDados, uClasseLogErros,SysUtils, Controls, uDM, IBQuery,Messages;

type
  TPessoa = class
  private
    Log: TLogErro;
    SQLExecute: TIBQuery;

    FDataCadastro: TDateTime; // Uses Controls
    FIDPessoa: Integer;
    FLogradouro_ID: String;
    FNome: string;
    FObservacao: string;
    FCPF: string;
    FSQLSelect: TIBQuery;
  public
    function GravarPessoa: Boolean;
    function ExcluirPessoa: Boolean;
    function ListarPessoas: Boolean;
    function ValidarPessoa: Boolean;
    function BuscarPessoa: Boolean;
    property IDPessoa: Integer read FIDPessoa write FIDPessoa;
    property Logradouro_ID: String read FLogradouro_ID write FLogradouro_ID;
    property DataCadastro: TDateTime read FDataCadastro write FDataCadastro;
    property Nome: string read FNome write FNome;
    property CPF: string read FCPF write FCPF;
    property Observacao: string read FObservacao write FObservacao;
    property SQLSelect: TIBQuery read FSQLSelect;
  end;

implementation

uses DB;

{ TPessoa }

function TPessoa.BuscarPessoa: Boolean;
begin
  Result := False;
  FSQLSelect := DM.BancoDeDados.CriaQuery;

  try
    FSQLSelect.SQL.Add('  SELECT                 ');
    FSQLSelect.SQL.Add('		CD_PESSOA,           ');
    FSQLSelect.SQL.Add('		NM_PESSOA,           ');
    FSQLSelect.SQL.Add('		DS_ENDERECO,         ');
    FSQLSelect.SQL.Add('		NR_ENDERECO,         ');
    FSQLSelect.SQL.Add('		DS_BAIRRO,           ');
    FSQLSelect.SQL.Add('		CD_ESTADO,           ');
    FSQLSelect.SQL.Add('		CD_CIDADE,           ');
    FSQLSelect.SQL.Add('		CD_PAIS,             ');
    FSQLSelect.SQL.Add('		CD_CEP,              ');
    FSQLSelect.SQL.Add('		DS_EMAIL,            ');
    FSQLSelect.SQL.Add('		NR_TELEFONE,         ');
    FSQLSelect.SQL.Add('		CD_CGCCPF,           ');
    FSQLSelect.SQL.Add('		CD_USUARIO,          ');
    FSQLSelect.SQL.Add('		DT_ALT,              ');
    FSQLSelect.SQL.Add('		HR_ALT,              ');
    FSQLSelect.SQL.Add('		DT_CAD,              ');
    FSQLSelect.SQL.Add('		HR_CAD,              ');
    FSQLSelect.SQL.Add('		CD_FILIAL            ');
    FSQLSelect.SQL.Add('FROM                     ');
    FSQLSelect.SQL.Add('		PESSOA_SIMPLES       ');
    FSQLSelect.SQL.Add('WHERE CD_PESSOA = :PID   ');
    FSQLSelect.ParamByName('PID').Value := FIDPessoa;

    try
      FSQLSelect.Open;

      if not(FSQLSelect.IsEmpty) then
      begin
        FLogradouro_ID := FSQLSelect.FieldByName('DS_ENDERECO').AsString;;
        FDataCadastro := FSQLSelect.FieldByName('DT_CAD').AsDateTime;
        FNome := FSQLSelect.FieldByName('NM_PESSOA').AsString;
        FCPF := FSQLSelect.FieldByName('CD_CGCCPF').AsString;
        FObservacao := FSQLSelect.FieldByName('DS_EMAIL').AsString;
        Result := True;
      end;
    except on e:Exception do
      begin
        Log := TLogErro.Create;

        try
          Log.Erro := e.Message;
          Log.Funcao := 'TPessoa.BuscarPessoas';
          Log.GravaLog;
        finally
          Log.Free;
        end;
      end;
    end;
  finally
    FSQLSelect.Free;
  end;
end;

function TPessoa.ExcluirPessoa: Boolean;
begin
  Result := False;
  SQLExecute := DM.BancoDeDados.CriaQuery;

  if not(DM.BancoDeDados.Transacao.InTransaction) then
    DM.BancoDeDados.Transacao.StartTransaction;

  try
    SQLExecute.SQL.Add('DELETE FROM PESSOA_SIMPLES');
    SQLExecute.SQL.Add('WHERE CD_PESSOA = :PID');
    SQLExecute.ParamByName('PID').Value := FIDPessoa;

    try
      SQLExecute.ExecSQL;
      DM.BancoDeDados.Transacao.Commit;
      Result := True;
    except on e:Exception do
      begin
        DM.BancoDeDados.Transacao.Rollback;

        Log := TLogErro.Create;

        try
          Log.Erro := e.Message;
          Log.Funcao := 'TPessoa.ExcluirPessoa';
          Log.GravaLog;
        finally
          Log.Free;
        end;
      end;
    end;
  finally
    SQLExecute.Free;
  end;
end;

function TPessoa.GravarPessoa: Boolean;
begin
  Result := False;
  SQLExecute := DM.BancoDeDados.CriaQuery;

  if not(DM.BancoDeDados.Transacao.InTransaction) then
    DM.BancoDeDados.Transacao.StartTransaction;

  if FIDPessoa = 0 then
  begin
    //ShowMessage('Programar o Update das Pessoas!');
  {
    SQLExecute.SQL.Add('Insert into Pessoa');
    SQLExecute.SQL.Add('(ID, Logradouro_Id,');
    SQLExecute.SQL.Add(' DataCadastro, Nome,');
    SQLExecute.SQL.Add(' CPF, Observacao)');
    SQLExecute.SQL.Add('Values');
    SQLExecute.SQL.Add('(:PID, :PLogradouro_Id,');
    SQLExecute.SQL.Add(' :PDataCadastro, :PNome,');
    SQLExecute.SQL.Add(' :PCPF, :PObservacao)');
  }
  end
  else
  begin
    //ShowMessage('Programar o Update das Pessoas!');
  {
    SQLExecute.SQL.Add('Update Pessoa Set');
    SQLExecute.SQL.Add('Logradouro_Id = :PLogradouro_Id,');
    SQLExecute.SQL.Add('DataCadastro = :PDataCadastro,');
    SQLExecute.SQL.Add('Nome = :PNome,');
    SQLExecute.SQL.Add('CPF = :PCPF,');
    SQLExecute.SQL.Add('Observacao = :PObservacao');
    SQLExecute.SQL.Add('Where ID = :PID');
   }
  end;
  {
  SQLExecute.ParamByName('PID').Value := FIDPessoa;
  SQLExecute.ParamByName('PLogradouro_Id').Value := FLogradouro_ID;
  SQLExecute.ParamByName('PDataCadastro').Value := FDataCadastro;
  SQLExecute.ParamByName('PNome').Value := FNome;
  SQLExecute.ParamByName('PCPF').Value := FCPF;
  SQLExecute.ParamByName('PObservacao').Value := FObservacao;
  }
  try
     //Descomentar Codigo abaixo quando tiver as funcoes
    //SQLExecute.ExecSQL;
    DM.BancoDeDados.Transacao.Commit;
    Result := True;
  except on e:Exception do
    begin
      DM.BancoDeDados.Transacao.Rollback;
      
      Log := TLogErro.Create;

      try
        Log.Erro := e.Message;
        Log.Funcao := 'TPessoa.GravarPessoa';
        Log.GravaLog;
      finally
        Log.Free;
      end;
    end;
  end;
end;

function TPessoa.ListarPessoas: Boolean;
begin
  Result := False;
  FSQLSelect := DM.BancoDeDados.CriaQuery;


    FSQLSelect.SQL.Add('  SELECT                 ');
    FSQLSelect.SQL.Add('		CD_PESSOA,           ');
    FSQLSelect.SQL.Add('		NM_PESSOA,           ');
    FSQLSelect.SQL.Add('		DS_ENDERECO,         ');
    FSQLSelect.SQL.Add('		NR_ENDERECO,         ');
    FSQLSelect.SQL.Add('		DS_BAIRRO,           ');
    FSQLSelect.SQL.Add('		CD_ESTADO,           ');
    FSQLSelect.SQL.Add('		CD_CIDADE,           ');
    FSQLSelect.SQL.Add('		CD_PAIS,             ');
    FSQLSelect.SQL.Add('		CD_CEP,              ');
    FSQLSelect.SQL.Add('		DS_EMAIL,            ');
    FSQLSelect.SQL.Add('		NR_TELEFONE,         ');
    FSQLSelect.SQL.Add('		CD_CGCCPF,           ');
    FSQLSelect.SQL.Add('		CD_USUARIO,          ');
    FSQLSelect.SQL.Add('		DT_ALT,              ');
    FSQLSelect.SQL.Add('		HR_ALT,              ');
    FSQLSelect.SQL.Add('		DT_CAD,              ');
    FSQLSelect.SQL.Add('		HR_CAD,              ');
    FSQLSelect.SQL.Add('		CD_FILIAL            ');
    FSQLSelect.SQL.Add('FROM                     ');
    FSQLSelect.SQL.Add('		PESSOA_SIMPLES       ');

  try
    FSQLSelect.Open;
    Result := True;
  except on e:Exception do
    begin
      Log := TLogErro.Create;

      try
        Log.Erro := e.Message;
        Log.Funcao := 'TPessoa.ListarPessoas';
        Log.GravaLog;
      finally
        Log.Free;
      end;
    end;
  end;
end;

function TPessoa.ValidarPessoa: Boolean;
begin
  Result := False;
  FSQLSelect := DM.BancoDeDados.CriaQuery;

  try

    FSQLSelect.SQL.Add('  SELECT                 ');
    FSQLSelect.SQL.Add('		CD_PESSOA,           ');
    FSQLSelect.SQL.Add('		NM_PESSOA,           ');
    FSQLSelect.SQL.Add('		DS_ENDERECO,         ');
    FSQLSelect.SQL.Add('		NR_ENDERECO,         ');
    FSQLSelect.SQL.Add('		DS_BAIRRO,           ');
    FSQLSelect.SQL.Add('		CD_ESTADO,           ');
    FSQLSelect.SQL.Add('		CD_CIDADE,           ');
    FSQLSelect.SQL.Add('		CD_PAIS,             ');
    FSQLSelect.SQL.Add('		CD_CEP,              ');
    FSQLSelect.SQL.Add('		DS_EMAIL,            ');
    FSQLSelect.SQL.Add('		NR_TELEFONE,         ');
    FSQLSelect.SQL.Add('		CD_CGCCPF,           ');
    FSQLSelect.SQL.Add('		CD_USUARIO,          ');
    FSQLSelect.SQL.Add('		DT_ALT,              ');
    FSQLSelect.SQL.Add('		HR_ALT,              ');
    FSQLSelect.SQL.Add('		DT_CAD,              ');
    FSQLSelect.SQL.Add('		HR_CAD,              ');
    FSQLSelect.SQL.Add('		CD_FILIAL            ');
    FSQLSelect.SQL.Add('FROM                     ');
    FSQLSelect.SQL.Add('		PESSOA_SIMPLES       ');
    FSQLSelect.SQL.Add('WHERE CD_CGCCPF = :PCPF  ');
    FSQLSelect.ParamByName('PCPF').Value := FCPF;

    try
      FSQLSelect.Open;

      if not(FSQLSelect.IsEmpty) then
      begin
        FLogradouro_ID := FSQLSelect.FieldByName('DS_ENDERECO').AsString;;
        FDataCadastro := FSQLSelect.FieldByName('DT_CAD').AsDateTime;
        FNome := FSQLSelect.FieldByName('NM_PESSOA').AsString;
        FCPF := FSQLSelect.FieldByName('CD_CGCCPF').AsString;
        FObservacao := FSQLSelect.FieldByName('DS_EMAIL').AsString;
        Result := True;
      end;
    except on e:Exception do
      begin
        Log := TLogErro.Create;

        try
          Log.Erro := e.Message;
          Log.Funcao := 'TPessoa.ValidarPessoa';
          Log.GravaLog;
        finally
          Log.Free;
        end;
      end;
    end;
  finally
    FSQLSelect.Free;
  end;
end;

end.
