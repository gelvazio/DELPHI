program ProjetoDA;

uses
  Forms,
  uClasseBancoDados in '..\Fontes\uClasseBancoDados.pas',
  uClasseLogErros in '..\Fontes\uClasseLogErros.pas',
  uClasseCriptografia in '..\Fontes\uClasseCriptografia.pas',
  uInicializaSistema in '..\Fontes\uInicializaSistema.pas' {fInicializaSistema},
  uConfiguraBanco in '..\Fontes\uConfiguraBanco.pas' {fConfiguraBanco},
  uLogOn in '..\Fontes\uLogOn.pas' {fLogOn},
  uClasseUsuario in '..\Fontes\uClasseUsuario.pas',
  uClassePessoa in '..\Fontes\uClassePessoa.pas',
  uDM in '..\Fontes\uDM.pas' {DM: TDataModule},
  uCadastroUsuario in '..\Fontes\uCadastroUsuario.pas' {fCadastroUsuario},
  uFrameBotoes in '..\Fontes\uFrameBotoes.pas' {fFrameBotoes: TFrame},
  uMenu in '..\Fontes\uMenu.pas' {fMenu},
  uPesquisaPessoa in '..\Fontes\uPesquisaPessoa.pas' {fPesquisaPessoa},
  uPesquisaUsuario in '..\Fontes\uPesquisaUsuario.pas' {fPesquisaUsuario},
  uFrameBotoesPesquisa in '..\Fontes\uFrameBotoesPesquisa.pas' {fFrameBotoesPesquisa: TFrame},
  uCadastroPessoa in '..\Fontes\uCadastroPessoa.pas' {fCadastroPessoa};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TDM, DM);
  Application.Run;
end.
