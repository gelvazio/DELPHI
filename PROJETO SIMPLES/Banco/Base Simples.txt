/* Server version: WI-V6.3.3.26778 Firebird 2.5 
   SQLDialect: 3. ODS: 11.2. Forced writes: On. Sweep inteval: 20000.
   Page size: 16384. Cache pages: 2048 (32768 Kb). Read-only: False. */
SET NAMES ISO8859_1;

SET SQL DIALECT 3;

CONNECT 'D:\PROGRAMACAO LOCAL\DADOS TEMP\BASE LIMPA.fdb' USER 'SYSDBA' PASSWORD 'masterkey';

SET AUTODDL ON;

/* Create Table... */
CREATE TABLE PESSOA(ID INTEGER NOT NULL,
LOGRADOURO_ID INTEGER,
DATACADASTRO DATE,
NOME VARCHAR(100),
CPF VARCHAR(14),
OBSERVACAO VARCHAR(200));


CREATE TABLE PESSOA_SIMPLES(CD_PESSOA INTEGER NOT NULL,
NM_PESSOA VARCHAR(400) NOT NULL COLLATE PT_BR,
DS_ENDERECO VARCHAR(400) COLLATE PT_BR,
NR_ENDERECO VARCHAR(50),
DS_BAIRRO VARCHAR(200),
CD_ESTADO VARCHAR(2),
CD_CIDADE INTEGER,
CD_PAIS INTEGER DEFAULT 1058,
CD_CEP VARCHAR(8),
DS_EMAIL VARCHAR(200),
NR_TELEFONE VARCHAR(200),
CD_CGCCPF VARCHAR(25) NOT NULL,
CD_USUARIO SMALLINT NOT NULL,
DT_ALT DATE NOT NULL,
HR_ALT TIME NOT NULL,
DT_CAD DATE NOT NULL,
HR_CAD TIME NOT NULL,
CD_FILIAL INTEGER NOT NULL);


CREATE TABLE USUARIO(ID INTEGER NOT NULL,
GRUPO_ID INTEGER,
MASTER INTEGER,
ATIVO INTEGER,
TIPOUSUARIO INTEGER,
SENHA VARCHAR(10));



/* Create generator... */
CREATE GENERATOR GEN_PESSOA_ID;


/* Create Primary Key... */
ALTER TABLE PESSOA ADD CONSTRAINT PK_PESSOA PRIMARY KEY (ID);

ALTER TABLE PESSOA_SIMPLES ADD CONSTRAINT PK_PESSOA_SIMPLES PRIMARY KEY (CD_PESSOA);

ALTER TABLE USUARIO ADD CONSTRAINT PK_USUARIO PRIMARY KEY (ID);

/* Create Foreign Key... */
RECONNECT;

ALTER TABLE USUARIO ADD CONSTRAINT FK_USUARIO_PESSOA FOREIGN KEY (ID) REFERENCES PESSOA (ID);

/* Creating trigger... */
SET TERM ^ ;

CREATE TRIGGER PESSOA_BI FOR PESSOA
ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.id = 0) then
    new.id = gen_id(gen_pessoa_id,1);
end
^


/* Create(Add) privilege */
SET TERM ; ^

GRANT ALL ON PESSOA TO SYSDBA WITH GRANT OPTION;

GRANT ALL ON PESSOA_SIMPLES TO SYSDBA WITH GRANT OPTION;

GRANT ALL ON USUARIO TO SYSDBA WITH GRANT OPTION;

